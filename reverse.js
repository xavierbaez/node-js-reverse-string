let str = 'olle.H';
let dot = '.';
console.log('original: ' + str);
let position = str.indexOf(dot);
console.log('position of dot: ' + position);
let reversedStr = '';
for (let i = str.length - 1; i >= 0; i--) {
    if (str[i]==='.') {
        continue;
    }
    reversedStr += str[i].toLowerCase();
}
console.log('The reversed: ' + reversedStr);
let reversedStrWithDot = [reversedStr.slice(0, position), dot, reversedStr.slice(position)].join('');
console.log('The reversed with dot: ' + reversedStrWithDot);